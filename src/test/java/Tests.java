import org.junit.jupiter.api.DisplayName;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;

@DisplayName("Test Suite")
@RunWith(JUnitPlatform.class)
@SelectPackages({
        "com.epam.tests.ApplicationTest",
        "com.epam.tests.LongestPlateauTest",
        "com.epam.tests.MinesweeperTest"
})
public class Tests {
    Result result = JUnitCore.runClasses();
}