package com.epam.tests;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

import com.epam.MineSweeper;

public class MineSweeperTest {
    int m = 5;
    int n = 6;
    double mineProbability = 0.4;
    private final MineSweeper minesweeper = new MineSweeper(
            m, n, mineProbability
    );

    @Test
    void assertCreated() {
        assertNotNull(minesweeper);
        assertNotNull(minesweeper.getPrintableField());
        assertNotNull(minesweeper.getInfoField());
    }

    @Disabled
    @Test
    void testFieldGeneration() throws NoSuchFieldException, IllegalAccessException {

        Field field = MineSweeper.class.getDeclaredField("mine");
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, false);

        MineSweeper flipped = new MineSweeper(m, n, mineProbability);
        assertNotNull(flipped);
    }

    @RepeatedTest(10)
    void testRandomization() {
        assertNotSame(minesweeper, new MineSweeper(
                m, n, mineProbability
        ));
    }
}
