package com.epam.tests;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.epam.LongestPlateau;


public class LongestPlateauTest {
    private final LongestPlateau longestPlateau = new LongestPlateau();

    @Test
    void assertInstantiated() {
        assertNotNull(longestPlateau);
    }

    @Test
    void findingLongestPlateauFirst() {
        int[] input = new int[]{1, 1, 1, 0, 2, 2, 0, 1};
        int[] expectedOutput = new int[]{1, 1, 1};
        int[] output = longestPlateau.find(input);

        assertArrayEquals(expectedOutput, output);
    }

    @Test
    void findingLongestPlateauMiddle() {
        int[] input = new int[]{1, 1, 0, 0, 2, 2, 2, 2, 0, 1};
        int[] expectedOutput = new int[]{2, 2, 2, 2};
        int[] output = longestPlateau.find(input);

        assertArrayEquals(expectedOutput, output);
    }

    @Test
    void findingLongestPlateauLast() {
        int[] input = new int[]{1, 1, -1, 0, 0, 2, 2, 3, 3, 3};
        int[] expectedOutput = new int[]{3, 3, 3};
        int[] output = longestPlateau.find(input);

        assertArrayEquals(expectedOutput, output);
    }

    @Test
    void findingLongestPlateauChallenged() {
        int[] input = new int[]{0, 1, 1, 1, 0, 0, 2, 2, 2, 2, 3};
        int[] expectedOutput = new int[]{1, 1, 1};
        int[] output = longestPlateau.find(input);

        assertArrayEquals(expectedOutput, output);
    }

    @Test
    void findingLongestPlateauEmpty() {
        int[] input = new int[0];
        int[] output = longestPlateau.find(input);

        assertFalse(output.length > input.length);
    }
}