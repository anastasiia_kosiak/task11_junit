package com.epam.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.epam.Handler;
import com.epam.Reader;
import com.epam.Writer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ApplicationTest {
    @BeforeAll
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @InjectMocks
    Handler dataHandler;

    @Mock
    Reader mockReader;

    @Mock
    Writer mockWriter;

    @Test
    public void copying() {
        boolean copied = dataHandler.copy("test.txt", "test.txt");
        assertTrue(copied);

        verify(mockReader, times(1)).read("test.txt");
        verify(mockWriter, times(1)).write(null, "test.txt");
    }
}
