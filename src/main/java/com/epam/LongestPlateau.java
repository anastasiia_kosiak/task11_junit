package com.epam;

import java.util.Arrays;

public class LongestPlateau {

    public int[] find(int[] all) {
        if (all.length == 0) {
            return new int[0];
        }
        int[] longestPlateau = new int[all.length];
        int[] currentPlateau = new int[all.length];

        int sizeLongest = 0;
        int nextIndexCurrent = 0;

        int previous;
        int current = all[0];

        for (int i = 0; i < all.length; i++) {
            previous = current;
            current = all[i];

            if (i == 0 || previous < current) {
                // plateau begins
                currentPlateau = new int[all.length - i];

                currentPlateau[0] = current;
                nextIndexCurrent = 1;
            }

            if (previous == current && i != 0) {
                // plateau continues
                currentPlateau[nextIndexCurrent] = current;
                nextIndexCurrent++;
            }

            if (previous > current || i == all.length - 1) {
                // plateau ends
                if (nextIndexCurrent > sizeLongest) {
                    sizeLongest = nextIndexCurrent;
                    longestPlateau = Arrays.copyOf(currentPlateau, sizeLongest);
                }
            }
        }

        return longestPlateau;
    }
}
