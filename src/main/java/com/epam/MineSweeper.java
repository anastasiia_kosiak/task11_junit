package com.epam;

public class MineSweeper {
    private int M;
    private int N;
    private final boolean[][] field;

    final static boolean mine = true;

    public MineSweeper(int M, int N, double p) {

        if (M <= 0 || N <= 0) {
            throw new IllegalArgumentException(
                    "Only positive integers allowed"
            );
        }
        this.M = M;
        this.N = N;
        field = new boolean[M + 2][N + 2];

        for (int i = 1; i < M; i++) {
            for (int j = 1; j < N; j++) {
                double r = Math.random();
                field[i][j] = r <= p;
            }
        }
    }

    public char[][] getPrintableField() {
        char[][] printableField = new char[M + 2][N + 2];

        for (int i = 1; i < M; i++) {
            for (int j = 1; j < N; j++) {
                if (field[i][j] == mine) {
                    printableField[i][j] = '*';
                } else {
                    printableField[i][j] = '.';
                }
            }
        }

        return printableField;
    }

    public int[][] getInfoField() {

        int[][] infoField = new int[M + 2][N + 2];
        for (int i = 1; i < M; i++) {
            for (int j = 1; j < N; j++) {
                infoField[i][j] = 0;
                if (field[i - 1][j - 1]) {
                    infoField[i][j]++;
                }
                if (field[i - 1][j]) {
                    infoField[i][j]++;
                }
                if (field[i - 1][j + 1]) {
                    infoField[i][j]++;
                }
                if (field[i][j - 1]) {
                    infoField[i][j]++;
                }
                if (field[i][j + 1]) {
                    infoField[i][j]++;
                }
                if (field[i + 1][j - 1]) {
                    infoField[i][j]++;
                }
                if (field[i + 1][j]) {
                    infoField[i][j]++;
                }
                if (field[i + 1][j + 1]) {
                    infoField[i][j]++;
                }
            }
        }
        return infoField;
    }
}
