package com.epam;

public class Handler {
    Reader reader = new Reader();
    Writer writer = new Writer();

    public boolean copy(String inputFilename, String outputFilename) {
        String data = reader.read(inputFilename);
        writer.write(data, outputFilename);

        return true;
    }
}
